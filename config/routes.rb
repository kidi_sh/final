Rails.application.routes.draw do


  namespace :admins do
    resources :purchases
  end

  get 'about' => 'home_about#index'
  get 'location' => 'home_location#index'

  resources :homeleads
  resources :helpdesks
  mount Ckeditor::Engine => '/ckeditor'
  ActiveAdmin.routes(self)
  devise_for :admin_user, ActiveAdmin::Devise.config
  resources :blogs
  get 'tags/:tag', to: 'blogs#index', as: "tag"
  resources :dashboard

  get "benefits/redeem_log/:id" => "benefits#redeem_log"

  resources :contacts, only: [:create]

  resources :forums do
    resources :comments
  end

  resources :community

  resources :enquiries
  resources :home

  resources :after_signup

  resources :profiles
  root 'home#index'

  namespace :admins do
    resources :dashboard
  end

  namespace :admins do
    root 'report#index'
  end

  namespace :admins do
    resources :rooms, :plans, :payments, :events, :companies, :bookings, :users, :report,:statistic, :invoices, :settings,:banners,:profiles,:benefits, :abouts, :testimonials,:contacts, :generators
    #hard coded to bypass invoices
    resources :suborders, :path => "invoices"

  end

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :bookings, :only => [:index,:show, :create, :update]
    end
  end


  #devise_for :admins
  devise_for :users, :controllers => {
    :registrations => "users/registrations",
    :sessions => "users/sessions" }
  devise_for :admins, controllers: { registrations: "admins/registrations" }
  #resources :rooms
  resources :plans
  resources :payments
  resources :events
  resources :bookings
  resources :purchases
  resources :invoices
  resources :benefits


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"


  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
