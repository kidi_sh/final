ActiveAdmin.register User do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#

filter :email
filter :subscibed
# filter :profile, as: :select, collection: Group.order(:name)

permit_params do
  permitted = [:email, :subscibed, :wallet]
  # permitted << :other if params[:action] == 'create' && current_user.admin?
  permitted
end

form do |f|
    f.inputs "users" do
        f.input :email
        f.input :subscibed
        f.input :wallet
    end
    f.button :Submit
end


end
