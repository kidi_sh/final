ActiveAdmin.register Blog do
  before_filter :only => [:show, :edit,:update, :destroy] do
    @blog = Blog.friendly.find(params[:id])
  end
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
permit_params do
  permitted = [:title, :description,:image, :meta_description, :all_tags]
  # permitted << :other if params[:action] == 'create' && current_user.admin?
  permitted
end

form do |f|
  f.inputs "Upload" do
    f.input :title, as: :string
    f.input :meta_description, as: :string
    f.input :all_tags, as: :string
    f.input :description, as: :ckeditor
    f.input :image, required: true, as: :file
  end
  f.actions
end


end
