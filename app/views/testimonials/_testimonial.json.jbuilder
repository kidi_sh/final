json.extract! testimonial, :id, :name, :title, :description, :created_at, :updated_at
json.url testimonial_url(testimonial, format: :json)