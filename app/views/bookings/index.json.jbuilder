#json.array! @bookings, partial: 'bookings/booking', as: :booking

json.array!(@bookings) do |booking|
  json.extract! booking, :id
  json.title booking.user.profile.company.name
  json.start booking.start_book
  if current_user.id == booking.user_id
    json.color '#6cd86a'
  else
    json.color '#f39c12'
  end
  if booking.description == "purchase"
    json.color 'red'
  end
  json.end booking.end_book
  json.url booking_url(booking, format: :html)
end
