json.extract! about, :id, :description, :admin_id, :created_at, :updated_at
json.url about_url(about, format: :json)