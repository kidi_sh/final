json.extract! enquiry, :id, :title, :description, :user_id, :created_at, :updated_at
json.url enquiry_url(enquiry, format: :json)