json.extract! homelead, :id, :fullname, :phone, :email, :notes, :created_at, :updated_at
json.url homelead_url(homelead, format: :json)