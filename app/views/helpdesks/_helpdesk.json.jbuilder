json.extract! helpdesk, :id, :full_name, :company, :phone, :email, :user_id, :note, :admin_id, :created_at, :updated_at
json.url helpdesk_url(helpdesk, format: :json)