json.extract! plan, :id, :name, :description, :price, :deposit, :period, :created_at, :updated_at
json.url plan_url(plan, format: :json)