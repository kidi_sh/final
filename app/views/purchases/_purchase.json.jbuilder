json.extract! purchase, :id, :plan_id, :payment_id, :created_at, :updated_at
json.url purchase_url(purchase, format: :json)