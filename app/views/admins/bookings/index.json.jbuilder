#json.array! @bookings, partial: 'bookings/booking', as: :booking

json.array!(@bookings) do |booking|
  json.extract! booking, :id
  json.title booking.user.profile.company.name
  json.start booking.start_book
  json.end booking.end_book
  json.url admins_booking_url(booking, format: :html)
end
