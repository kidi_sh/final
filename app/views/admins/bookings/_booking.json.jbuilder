json.extract! booking, :id, :user_id, :room_id, :description, :start_book, :end_book, :created_at, :updated_at
json.url booking_url(booking, format: :json)