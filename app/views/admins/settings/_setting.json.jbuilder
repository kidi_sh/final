json.extract! setting, :id, :name, :address, :phone, :admin_id, :created_at, :updated_at
json.url setting_url(setting, format: :json)