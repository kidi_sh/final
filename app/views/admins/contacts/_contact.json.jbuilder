json.extract! contact, :id, :firstname, :lastname, :phone, :email, :enquiry, :created_at, :updated_at
json.url contact_url(contact, format: :json)