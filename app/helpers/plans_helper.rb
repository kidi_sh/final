module PlansHelper
  def options_for_period
    [
      ['Monthly','1'],
      ['Weekly','2'],
      ['Daily','3'],
      ['Hourly','4'],
      ['Event','5'],
      ['Event Space','6'],
      ['Product','7']
    ]
  end
end
