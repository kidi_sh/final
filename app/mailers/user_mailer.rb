class UserMailer < ApplicationMailer

  def notify_finance(invoice)
    @invoice = invoice
    if @invoice.purchase.user.admin.subdomain == "themaja"
      mail(:to => "alika@evhive.co", :cc => "mahardika@evhive.co", :subject => "Transaction approved from #{@invoice.purchase.user.email}") do |format|
        format.text { render text: "Please generate report/invoice for the below details.\nName: #{@invoice.purchase.user.profile.fullname}\nCompany: #{@invoice.purchase.user.profile.company.name}\nStart date: #{@invoice.start_date}\nEnd date:#{@invoice.start_date}\n generate the transaction at evhive.info.\n\nThank You \nSYSTEM EV HIVE" }
      end
    elsif @invoice.purchase.user.admin.subdomain == "breeze"
      mail(:to => "alika@evhive.co", :cc => "jesslyn@evhive.co", :subject => "Transaction approved from #{@invoice.purchase.user.email}") do |format|
        format.text { render text: "Please generate report/invoice for the below details.\nName: #{@invoice.purchase.user.profile.fullname}\nCompany: #{@invoice.purchase.user.profile.company.name}\nStart date: #{@invoice.start_date}\nEnd date:#{@invoice.start_date}\n generate the transaction at evhive.info.\n\nThank You \nSYSTEM EV HIVE" }
      end
    end
  end

  def notify_facility(purchase)
    @purchase =  purchase
    if @purchase.user.admin.subdomain == "themaja"
      mail(:to => "mahardika@evhive.co", :cc => "niken@evhive.co", :subject => "New purchase from #{@purchase.user.email}") do |format|
        format.text { render text: "Please kindly process the transaction from #{@purchase.user.email}.\nTo be process further to the finance department.\n\nThank You \nEV HIVE" }
      end
    elsif @purchase.user.admin.subdomain == "breeze"
      mail(:to => "jesslyn@evhive.co", :cc => "niken@evhive.co", :subject => "New purchase from #{@purchase.user.email}") do |format|
        format.text { render text: "Please kindly process the transaction from #{@purchase.user.email}.\nTo be process further to the finance department.\n\nThank You \nEV HIVE" }
      end
    end
  end

  def contact_facility(contact)
    @contact = contact
    if @contact.admin.subdomain == "themaja"
      mail(:to => "mahardika@evhive.co", :cc => "niken@evhive.co", :subject => "New lead from #{@contact.firstname}") do |format|
        format.text { render text: "New inbound lead from #{@contact.firstname}.\nPlease check the administator area.\n\nThank You \nEV HIVE" }
      end
    elsif @contact.admin.subdomain == "breeze"
      mail(:to => "jesslyn@evhive.co", :cc => "niken@evhive.co", :subject => "New lead from #{@contact.firstname}") do |format|
          format.text { render text: "New inbound lead from #{@contact.firstname}.\nPlease check the administator area.\n\nThank You \nEV HIVE" }
      end
    end
  end

  def notify_guest(helpdesk)
    @helpdesk = helpdesk
    if @helpdesk.admin.subdomain == "themaja"
      mail(:to => "mahardika@evhive.co", :cc => "niken@evhive.co", :subject => "New Guest from #{@helpdesk.full_name}") do |format|
        format.text { render text: "New guest: #{@helpdesk.full_name}.\n\nThank You \nEV HIVE" }
      end
      if @helpdesk.user.present?
      mail(:to => "#{@helpdesk.user.email} ", :subject => "You have new guest: #{@helpdesk.full_name}") do |format|
        format.text { render text: "Please meet your guest at the frontdesk.\nName: #{@helpdesk.full_name}.\nNotes: #{@helpdesk.note}\n\nThank You \nEV HIVE" }
      end
    end
  elsif @helpdesk.admin.subdomain == "breeze"
      mail(:to => "jesslyn@evhive.co", :cc => "niken@evhive.co", :subject => "New Guest from #{@helpdesk.full_name}") do |format|
          format.text { render text: "New guest: #{@helpdesk.full_name}.\n\nThank You \nEV HIVE" }
      end
      if @helpdesk.user.present?
      mail(:to => "#{@helpdesk.user.email} ", :subject => "You have new guest: #{@helpdesk.full_name}") do |format|
        format.text { render text: "Please meet your guest at the frontdesk.\nName: #{@helpdesk.full_name}.\nNotes: #{@helpdesk.note}\n\nThank You \nEV HIVE" }
      end
    end
    end
  end

  def home_lead(homelead)
    @homelead = homelead
    mail(:to => "info@evhive.co", :subject => "New home leads from #{@homelead.fullname}") do |format|
      format.text { render text: "New leads: #{@homelead.fullname}.\nphone: #{@homelead.phone}\nemail: #{@homelead.email}\nnotes:#{@homelead.notes}\n\nThank You \nEV HIVE" }
    end

  end

end
