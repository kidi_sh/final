class PurchasesController < ApplicationController
  #before_action :authenticate_user!
  before_action :authenticate_user!, :except => [:show, :index]
  before_action :set_purchase, only: [:show, :thanks]
  # GET /bookings
  # GET /bookings.json
  def index
    #@bookings = Booking.all
    @plans = Plan.all
    @puchases = Purchase.all
    @payments = Payment.all
    #@invoices = current_user.suborders.where('paid = ? and suborders.start_date between ? and ?',false, Date.today.beginning_of_month, Date.today.end_of_month)

  end

  # GET /bookings/1
  # GET /bookings/1.json
  def show
  end

  def thanks
  end

  # GET /bookings/new
  def new
    @plan = Plan.find(params[:plan])
    @purchase = Purchase.new
  end

  def create
    @purchase = Purchase.new(purchase_params)
    @purchase.user_id=current_user.id
    @plan =  Plan.find(params[:purchase][:plan_id])

    if @purchase.payment.name == "Point"
      sd = Date.parse(params[:purchase][:start_date])

      if @plan.period == 3 || @plan.period == 5
        if current_user.profile.company.name != "personal"
          cid = current_user.profile.company.id
          @company = Company.find(cid)
          cwall = @company.wallet.to_i
          iprice = @plan.price.to_i * @purchase.total_month / 50000
          if cwall > 0
            if cwall >= iprice
              lbal = cwall - iprice
              @company.update_attribute :wallet, lbal.to_i
              respond_to do | format |
                if @purchase.save
                  if_day_paid
                  current_user.update_attribute :subscibed, true
                  format.html { redirect_to @purchase, notice: 'Plan was successfully created.' }
                  format.json { render :show, status: :created, location: @purchase }
                else
                  format.html { render :new }
                  format.json { render json: @purchase.errors, status: :unprocessable_entity }
                end
              end
            else
              uwall = current_user.wallet
              if uwall > 0
                if uwall >= iprice
                  lbal = uwall - iprice
                  current_user.update_attribute :wallet, lbal.to_i
                  respond_to do | format |
                    if @purchase.save
                      if_day_paid
                      current_user.update_attribute :subscibed, true
                      format.html { redirect_to @purchase, notice: 'Plan was successfully created.' }
                      format.json { render :show, status: :created, location: @purchase }
                    else
                      format.html { render :new }
                      format.json { render json: @purchase.errors, status: :unprocessable_entity }
                    end
                  end
                end
              else
                redirect_to :back , notice: 'insufficience credit'
              end
            end
          else
            redirect_to :back , notice: 'insufficience credit'
          end
        end
      end
    else
      sd = Date.parse(params[:purchase][:start_date])
    respond_to do |format|
      if @purchase.save
        # profile = company
        if current_user.profile.company.name != "personal"
          if @plan.period == 1
            if_month_personal(sd)
          elsif @plan.period == 2
            if_week
          elsif @plan.period == 3
            if_day
          elsif @plan.period == 4
            if_hour
          elsif @plan.period == 6
            if_event_space
          elsif @plan.period == 7
            if_product
          end
        else
          # profile = personal
          if @plan.period == 1
            if_month_personal(sd)
          elsif @plan.period == 2
            if_week
          elsif @plan.period == 3
            if_day
          elsif @plan.period == 4
            if_hour
          elsif @plan.period == 6
            if_event_space
          elsif @plan.period == 7
            if_product
          end
        end
        UserMailer.notify_facility(@purchase).deliver_now
        format.html { redirect_to @purchase, notice: 'Plan was successfully created.' }
        format.json { render :show, status: :created, location: @purchase }
      else
        format.html { render :new }
        format.json { render json: @purchase.errors, status: :unprocessable_entity }
      end
    end
    end
  end

  def get_payment
    @payment = Payment.find(params[:id])
    respond_to do |format|
       format.js {  }
    end
  end

  def is_company(current_user)
    if current_user.profile.company.name == "personal"
      to_personal()
    else
      to_company()
    end
  end

  def to_personal

  end

  def to_company

  end

  def if_month_personal(sd)
    for i in 0..@purchase.total_month do
      if i == 0
        @suborder = Suborder.new()
        @suborder.start_date = DateTime.parse(params[:purchase][:start_date])
        @suborder.end_date = DateTime.parse(params[:purchase][:start_date]).end_of_month
        @suborder.total = (@plan.price * (sd..sd.end_of_month).count {|day| !day.saturday? && !day.sunday? } / (sd.beginning_of_month..sd.end_of_month).count {|day| !day.saturday? && !day.sunday? } ).round(-2)
        @suborder.occupancy = (sd..sd.end_of_month).count {|day| !day.saturday? && !day.sunday? }.to_f / (sd.beginning_of_month..sd.end_of_month).count {|day| !day.saturday? && !day.sunday? }.to_f
        @suborder.purchase_id = @purchase.id.round
        @suborder.product_name = @purchase.plan.product_name
        @suborder.save
      elsif i == @purchase.total_month
        #if !Date.parse(params[:purchase][:start_date]).next_month(i).beginning_of_month == Date.parse(params[:purchase][:start_date]).next_month(i)
          @suborder = Suborder.new()
          @suborder.start_date = DateTime.parse(params[:purchase][:start_date]).next_month(i).beginning_of_month
          @suborder.end_date = DateTime.parse(params[:purchase][:start_date]).next_month(i)
          @suborder.total = (@plan.price * (sd.next_month(i).beginning_of_month..sd.next_month(i)).count {|day| !day.saturday? && !day.sunday? } / (sd.next_month(i).beginning_of_month..sd.next_month(i).end_of_month).count {|day| !day.saturday? && !day.sunday? } ).round(-2)
          @suborder.occupancy = (sd.next_month(i).beginning_of_month..sd.next_month(i)).count {|day| !day.saturday? && !day.sunday? }.to_f / (sd.next_month(i).beginning_of_month..sd.next_month(i).end_of_month).count {|day| !day.saturday? && !day.sunday? }.to_f
          @suborder.purchase_id = @purchase.id
          @suborder.product_name = @purchase.plan.product_name
          @suborder.save
        #end
      else
        @suborder = Suborder.new()
        @suborder.start_date = DateTime.parse(params[:purchase][:start_date]).next_month(i).beginning_of_month
        @suborder.end_date = DateTime.parse(params[:purchase][:start_date]).next_month(i).end_of_month
        @suborder.total = @plan.price
        @suborder.occupancy = 1
        @suborder.purchase_id = @purchase.id
        @suborder.product_name = @purchase.plan.product_name
        @suborder.save
      end
    end
  end

  def if_month_company(sd)
    for i in 0..@purchase.total_month do
      if i == 0
        @suborder = Suborder.new()
        @suborder.start_date = DateTime.parse(params[:purchase][:start_date])
        @suborder.end_date = DateTime.parse(params[:purchase][:start_date]).end_of_month
        @suborder.total = @plan.price
        @suborder.occupancy = (sd..sd.end_of_month).count {|day| !day.saturday? && !day.sunday? }.to_f / (sd.beginning_of_month..sd.end_of_month).count {|day| !day.saturday? && !day.sunday? }.to_f
        @suborder.product_name = @purchase.plan.product_name
        @suborder.purchase_id = @purchase.id.round
        @suborder.save
      elsif i == @purchase.total_month
        #if !Date.parse(params[:purchase][:start_date]).next_month(i).beginning_of_month == Date.parse(params[:purchase][:start_date]).next_month(i)
          @suborder = Suborder.new()
          @suborder.start_date = DateTime.parse(params[:purchase][:start_date]).next_month(i).beginning_of_month
          @suborder.end_date = DateTime.parse(params[:purchase][:start_date]).next_month(i)
          @suborder.total = 0
          @suborder.occupancy = (sd.next_month(i).beginning_of_month..sd.next_month(i)).count {|day| !day.saturday? && !day.sunday? }.to_f / (sd.next_month(i).beginning_of_month..sd.next_month(i).end_of_month).count {|day| !day.saturday? && !day.sunday? }.to_f
          @suborder.product_name = @purchase.plan.product_name
          @suborder.purchase_id = @purchase.id
          @suborder.save
        #end
      else
        @suborder = Suborder.new()
        @suborder.start_date = DateTime.parse(params[:purchase][:start_date]).next_month(i).beginning_of_month
        @suborder.end_date = DateTime.parse(params[:purchase][:start_date]).next_month(i).end_of_month
        @suborder.total = @plan.price
        @suborder.occupancy = 1
        @suborder.product_name = @purchase.plan.product_name
        @suborder.purchase_id = @purchase.id
        @suborder.save
      end
    end
  end

  def if_day
    @suborder = Suborder.new()
    @suborder.start_date = DateTime.parse(params[:purchase][:start_date])
    @suborder.end_date = DateTime.parse(params[:purchase][:start_date]) + @purchase.total_month.days
    @suborder.total = @plan.price * @purchase.total_month
    @suborder.occupancy = @purchase.total_month
    @suborder.product_name = @purchase.plan.product_name
    @suborder.purchase_id = @purchase.id
    @suborder.save
  end

  def if_day_paid
    @suborder = Suborder.new()
    @suborder.start_date = DateTime.parse(params[:purchase][:start_date])
    @suborder.end_date = DateTime.parse(params[:purchase][:start_date]) + @purchase.total_month.days
    @suborder.total = @plan.price * @purchase.total_month
    @suborder.occupancy = @purchase.total_month
    @suborder.product_name = @purchase.plan.product_name
    @suborder.purchase_id = @purchase.id
    @suborder.paid = true
    @suborder.save
  end

  def if_week
    @suborder = Suborder.new()
    @suborder.start_date = DateTime.parse(params[:purchase][:start_date])
    @suborder.end_date = DateTime.parse(params[:purchase][:start_date]).next_week
    @suborder.total = @plan.price
    @suborder.occupancy = @purchase.total_month
    @suborder.product_name = @purchase.plan.product_name
    @suborder.purchase_id = @purchase.id
    @suborder.save
  end

  def if_hour
    if !@purchase.person.nil?
      @suborder = Suborder.new()
      @suborder.start_date = DateTime.parse(params[:purchase][:start_date])
      @suborder.end_date = DateTime.parse(params[:purchase][:start_date]).end_of_day
      @suborder.total = @plan.extracharge * @purchase.person * (params[:purchase][:total_month]).to_i
      @suborder.occupancy = params[:purchase][:person]
      @suborder.product_name = "[The Maja] Meeting Room Additional Head Count"
      @suborder.purchase_id = @purchase.id
      @suborder.save
    end
    cd = DateTime.parse(params[:purchase][:start_date])
    if cd.saturday? == true || cd.sunday? == true
      @suborder = Suborder.new()
      @suborder.start_date = DateTime.parse(params[:purchase][:start_date])
      @suborder.end_date = DateTime.parse(params[:purchase][:start_date]) + (params[:purchase][:total_month]).to_i.hours
      @suborder.total = @plan.weekend_price * (params[:purchase][:total_month]).to_i
      @suborder.occupancy = @purchase.total_month
      @suborder.product_name = @purchase.plan.weekend_name
      @suborder.purchase_id = @purchase.id
      @suborder.save
    else
      @suborder = Suborder.new()
      @suborder.start_date = DateTime.parse(params[:purchase][:start_date])
      @suborder.end_date = DateTime.parse(params[:purchase][:start_date]) + (params[:purchase][:total_month]).to_i.hours
      @suborder.total = @plan.price * (params[:purchase][:total_month]).to_i
      @suborder.occupancy = @purchase.total_month
      @suborder.product_name = @purchase.plan.product_name
      @suborder.purchase_id = @purchase.id
      @suborder.save
    end
    @booking = Booking.new()
    @booking.user_id = current_user.id
    @booking.room_id = params[:purchase][:room_id]
    @booking.description = "purchase"
    @booking.start_book = DateTime.parse(params[:purchase][:start_date])
    @booking.end_book = DateTime.parse(params[:purchase][:start_date]) + (params[:purchase][:total_month]).to_i.hours
    @booking.save(validate: false)
  end

  def if_event_space
    if !@purchase.person.nil?
      @suborder = Suborder.new()
      @suborder.start_date = DateTime.parse(params[:purchase][:start_date])
      @suborder.end_date = DateTime.parse(params[:purchase][:start_date]).end_of_day
      @suborder.total = @plan.extracharge * @purchase.person * (params[:purchase][:total_month]).to_i
      @suborder.occupancy = params[:purchase][:person]
      @suborder.product_name = "[The Maja] Meeting Room Additional Head Count"
      @suborder.purchase_id = @purchase.id
      @suborder.save
    end
    cd = DateTime.parse(params[:purchase][:start_date])
    if cd.saturday? == true || cd.sunday? == true
      @suborder = Suborder.new()
      @suborder.start_date = DateTime.parse(params[:purchase][:start_date])
      @suborder.end_date = DateTime.parse(params[:purchase][:start_date]) + (params[:purchase][:total_month]).to_i.hours
      @suborder.total = @plan.weekend_price * (params[:purchase][:total_month]).to_i
      @suborder.occupancy = @purchase.total_month
      @suborder.product_name = @purchase.plan.weekend_name
      @suborder.purchase_id = @purchase.id
      @suborder.save
    else
      @suborder = Suborder.new()
      @suborder.start_date = DateTime.parse(params[:purchase][:start_date])
      @suborder.end_date = DateTime.parse(params[:purchase][:start_date]) + (params[:purchase][:total_month]).to_i.hours
      @suborder.total = @plan.price * (params[:purchase][:total_month]).to_i
      @suborder.occupancy = @purchase.total_month
      @suborder.product_name = @purchase.plan.product_name
      @suborder.purchase_id = @purchase.id
      @suborder.save
    end
  end

  def if_product
    @suborder = Suborder.new()
    @suborder.start_date = DateTime.parse(params[:purchase][:start_date])
    @suborder.end_date = DateTime.parse(params[:purchase][:start_date]).next_year
    @suborder.total = @plan.price
    @suborder.occupancy = @purchase.total_month
    @suborder.product_name = @purchase.plan
    @suborder.purchase_id = @purchase.id
    @suborder.save
  end

  private

  def set_purchase
    @purchase = Purchase.find(params[:id])
  end



  def purchase_params
    params.require(:purchase).permit(:user_id, :plan_id, :payment_id, :notes, :start_date, :total_month, :person, :room_id, :event_id)
  end




  # GET /bookings/1/edit

end
