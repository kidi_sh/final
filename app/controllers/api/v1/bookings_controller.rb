class Api::V1::BookingsController < Api::V1::BaseController
  respond_to :json
#get
  def index
    headers['Access-Control-Allow-Origin'] = "*"
    respond_with Booking.all.map{|f|{:title => f.user.profile.company.name, :start => f.start_book, :end => f.end_book}}
  end
#get
  def show
    respond_with Booking.find(params[:id])
  end
#post
  def create
  @bookings = Booking.new(booking_params)

  if @bookings.save
    render json: @bookings, status: 201
  else
    render json: { errors: @bookings.errors}, status: 422
  end
  end


  private

  def booking_params
    params.require(:booking).permit(:user_id, :room_id, :description, :start_book, :end_book)
  end



end
