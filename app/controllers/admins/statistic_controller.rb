class Admins::StatisticController < ApplicationController
  layout 'admin'
  before_filter :authenticate_admin!
  def index
    @suborders = Suborder.where("paid = ?", true)
    @bookings = Booking.all
  end
end
