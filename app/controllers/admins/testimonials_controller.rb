class Admins::TestimonialsController < TestimonialsController
  layout 'admin'

  def update
    respond_to do |format|
      if @testimonial.update(testimonial_params)
        format.html { redirect_to [:admins,@testimonial], notice: 'About was successfully updated.' }
        format.json { render :show, status: :ok, location: [:admins,@testimonial] }
      else
        format.html { render :edit }
        format.json { render json: @testimonial.errors, status: :unprocessable_entity }
      end
    end
  end

  def create
    @testimonial = Testimonial.new(testimonial_params)

    respond_to do |format|
      if @testimonial.save
        format.html { redirect_to [:admins,@testimonial], notice: 'About was successfully created.' }
        format.json { render :show, status: :created, location: [:admins,@testimonial] }
      else
        format.html { render :new }
        format.json { render json: @testimonial.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @testimonial.destroy
    respond_to do |format|
      format.html { redirect_to admins_testimonials_url, notice: 'About was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
end
