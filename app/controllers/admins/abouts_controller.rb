class Admins::AboutsController < AboutsController
  layout 'admin'

  def update
    respond_to do |format|
      if @about.update(about_params)
        format.html { redirect_to [:admins,@about], notice: 'About was successfully updated.' }
        format.json { render :show, status: :ok, location: [:admins,@about] }
      else
        format.html { render :edit }
        format.json { render json: @about.errors, status: :unprocessable_entity }
      end
    end
  end

  def create
    @about = About.new(about_params)

    respond_to do |format|
      if @about.save
        format.html { redirect_to [:admins,@about], notice: 'About was successfully created.' }
        format.json { render :show, status: :created, location: [:admins,@about] }
      else
        format.html { render :new }
        format.json { render json: @about.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @about.destroy
    respond_to do |format|
      format.html { redirect_to admins_abouts_url, notice: 'About was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
end
