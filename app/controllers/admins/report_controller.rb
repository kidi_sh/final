class Admins::ReportController < ApplicationController
	before_filter :authenticate_admin!
	layout	'admin'
	def index
		@count = Suborder.where('start_date BETWEEN ? AND ?', Date.today.beginning_of_month, Date.today.end_of_month ).count
		@sum = Suborder.where('start_date between ? and ?', Date.today.beginning_of_month, Date.today.end_of_month  ).sum(:total)
		#paid
		@suborders = Suborder.where('paid = ? and suborders.start_date >= ? and  suborders.start_date <= ?',false, Date.today.beginning_of_month, Date.today.end_of_month.end_of_day ).order(created_at: :desc)
		@paids = Suborder.where('paid = ? and suborders.start_date between ? and ?',true, Date.today.beginning_of_month, Date.today.end_of_month)

		@next_months = Suborder.where('paid = ? and suborders.start_date >= ? and  suborders.start_date <= ?',false, Date.today.next_month.beginning_of_month, Date.today.next_month.end_of_month.end_of_day ).order(created_at: :desc)
	end
end
