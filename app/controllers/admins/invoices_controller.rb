class Admins::InvoicesController < ApplicationController
  layout 'admin'
  before_action :set_invoice, only: [:create,:show, :edit, :update, :destroy]
  before_action :authenticate_admin!

  def index
  end

  def show
  end

  def edit
  end

  def new
    @invoice = Suborder.new
  end

  def update
    respond_to do |format|
      if @invoice.update(invoice_params)
        if @invoice.paid == true
          user = User.find(@invoice.purchase.user.id)
          user.update_attribute :subscibed, true

          tag = current_space.setting.tag.to_s
          # API to mware
          require 'uri'
          require 'net/http'

          if Rails.env.production?
            url = URI("https://evhive.info/api/v1/suborders/")
          else
            url = URI("http://mware.dev/api/v1/suborders/")
          end

          http = Net::HTTP.new(url.host, url.port)
          if Rails.env.production?
          http.use_ssl = true
          else
          http.use_ssl = false
          end
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE

          if @invoice.purchase.user.profile.company.name != "personal"
            @payload = {
                        "start_date": @invoice.start_date,
                        "end_date": @invoice.end_date,
                        "total": @invoice.total,
                        "days": @invoice.days,
                        "product_name": @invoice.product_name,
                        "company": @invoice.purchase.user.profile.company.nama_pt,
                        "center": tag
                      }.to_json
          else
            @payload = {
                        "start_date": @invoice.start_date,
                        "end_date": @invoice.end_date,
                        "total": @invoice.total,
                        "days": @invoice.days,
                        "product_name": @invoice.product_name,
                        "company": @invoice.purchase.user.profile.fullname,
                        "email": @invoice.purchase.user.email,
                        "center": tag
                      }.to_json
          end

          request = Net::HTTP::Post.new(url)
          request["content-type"] = 'application/json'
          request.body = @payload

          response = http.request(request)
          puts response.read_body

          UserMailer.notify_finance(@invoice).deliver_now

        end

        format.html { redirect_to [:admins,@invoice], notice: 'Booking was successfully updated.' }
        format.json { render :show, status: :ok, location: [ ] }
      else
        format.html { render :edit }
        format.json { render json: @invoice.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @invoice.destroy
    respond_to do |format|
      format.html { redirect_to admins_root_path, notice: 'invoice was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

 private
    # Use callbacks to share common setup or constraints between actions.
  def set_invoice
    @invoice = Suborder.find(params[:id])
  end

  def invoice_params
    params.require(:suborder).permit(:start_date, :end_date, :total, :paid)
  end
end
