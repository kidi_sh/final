class Admins::GeneratorsController < ApplicationController
  layout 'admin'
  def index
    @generators = Generator.all.order(id: :desc)
  end
  def new
    @generator = Generator.new
    @plans = Plan.uniq.pluck(:name)
  end

  def create
    @generator = Generator.create(gerenator_params)
    redirect_to [:admins,@generator]
  end

  def show
    @generator = Generator.find(params[:id])
  end

  private

  def gerenator_params
    params.require(:generator).permit(:plan_name,:start_date,:end_date)
  end
end
