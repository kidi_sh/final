class Admins::PurchasesController < ApplicationController
  layout 'admin'
  def index
    @plan = Plan.where("period = 6")
  end
  def new
    @purchase = Purchase.new
    @plan = Plan.find(params[:plan])
    @user = User.new
    @user.build_profile
  end

  def create
    @purchase = Purchase.new(purchase_params)
    @plan =  Plan.find(params[:purchase][:plan_id])

    respond_to do |format|
      if @purchase.save
        if @plan.period == 6
            if_event_space
        end
        format.html { redirect_to admins_root_path, notice: 'Plan was successfully created.' }
        format.json { render :show, status: :created, location: admins_root_path }
      else
        format.html { render :new }
        format.json { render json: admins_root_path, status: :unprocessable_entity }
      end
    end
  end




    def if_event_space
      if !@purchase.person.nil?
        @suborder = Suborder.new()
        @suborder.start_date = DateTime.parse(params[:purchase][:start_date])
        @suborder.end_date = DateTime.parse(params[:purchase][:start_date]).end_of_day
        @suborder.total = @plan.extracharge * @purchase.person * (params[:purchase][:total_month]).to_i
        @suborder.occupancy = params[:purchase][:person]
        @suborder.product_name = "[The Maja] Meeting Room Additional Head Count"
        @suborder.purchase_id = @purchase.id
        @suborder.save
      end
      cd = DateTime.parse(params[:purchase][:start_date])
      if cd.saturday? == true || cd.sunday? == true
        @suborder = Suborder.new()
        @suborder.start_date = DateTime.parse(params[:purchase][:start_date])
        @suborder.end_date = DateTime.parse(params[:purchase][:start_date]) + (params[:purchase][:total_month]).to_i.hours
        @suborder.total = @plan.weekend_price * (params[:purchase][:total_month]).to_i
        @suborder.occupancy = @purchase.total_month
        @suborder.product_name = @purchase.plan.weekend_name
        @suborder.purchase_id = @purchase.id
        @suborder.save
      else
        @suborder = Suborder.new()
        @suborder.start_date = DateTime.parse(params[:purchase][:start_date])
        @suborder.end_date = DateTime.parse(params[:purchase][:start_date]) + (params[:purchase][:total_month]).to_i.hours
        @suborder.total = @plan.price * (params[:purchase][:total_month]).to_i
        @suborder.occupancy = @purchase.total_month
        @suborder.product_name = @purchase.plan.product_name
        @suborder.purchase_id = @purchase.id
        @suborder.save
      end
    end

  private
    # Use callbacks to share common setup or constraints between actions.
    def purchase_params
      params.require(:purchase).permit(:user_id, :plan_id, :payment_id, :notes, :start_date, :total_month, :person, :room_id, :event_id)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:email, :password, :password_confirmation, profile_attribute: [:fullname])
    end

end
