class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  before_filter :configure_permitted_parameters, if: :devise_controller?
  around_filter :scope_current_space
  layout :layout_by_resource
  protect_from_forgery with: :exception
  # before_filter :find_subdomain

  def layout_by_resource
    if devise_controller? && resource_name == :admin && action_name == 'edit'
      "admin"
    else
      "application"
    end
  end

  def subs?
  current_user.try(:subscibed?)
  end
  helper_method :subs?

  private

  def current_space
  	Admin.find_by_subdomain(request.subdomain)
  end
  helper_method :current_space

  def scope_current_space
    case request.subdomain
        when /^www$|^$/

        else
          Admin.current_id = current_space.id
    end
    yield
    ensure
    Admin.current_id = nil
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:subdomain])
    devise_parameter_sanitizer.permit(:account_update, keys: [:subdomain])
  end

end
