class BookingsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_booking, only: [:show, :edit, :update, :destroy]

  # GET /bookings
  # GET /bookings.json
  def index
    @booking = Booking.new
    respond_to do | format |
    #@bookings = Booking.all
      format.html { @bookings = current_user.bookings.all.page params[:page]  }
      format.json { @bookings = Booking.all }
    end
  end

  # GET /bookings/1
  # GET /bookings/1.json
  def show
  end

  # GET /bookings/new
  def new
    @booking = Booking.new
  end

  # GET /bookings/1/edit
  def edit
  end

  # POST /bookings
  # POST /bookings.json
  def create
    @booking = Booking.new(booking_params)
    @booking.user_id=current_user.id
    @booking.admin_id= current_space.id
    ebook = @booking.end_book.to_i
    sbook = @booking.start_book.to_i
    es =  ebook - sbook
    if current_user.profile.company.name != "personal"
      cwall =  current_user.profile.company.id
      @company = Company.find(cwall)
      if (@company.wallet.to_i - (es*3/3600).to_i) > 0
        nbalance = @company.wallet.to_i - (es*3/3600).to_i
        @company.update_attribute :wallet, nbalance.to_i
        respond_to do |format|
          if @booking.save
            format.html { redirect_to bookings_url, notice: 'Booking was successfully created.' }
            format.json { render :show, status: :created, location: @booking }
          else
            format.html { render :new }
            format.json { render json: @booking.errors, status: :unprocessable_entity }
          end
        end
      elsif (current_user.wallet.to_i - (es*3/3600).to_i) > 0
        nbalance = current_user.wallet.to_i - (es*3/3600).to_i
        current_user.update_attribute :wallet, nbalance.to_i
        respond_to do |format|
          if @booking.save
            format.html { redirect_to bookings_url, notice: 'Booking was successfully created.' }
            format.json { render :show, status: :created, location: @booking }
          else
            format.html { render :new }
            format.json { render json: @booking.errors, status: :unprocessable_entity }
          end
        end
      else
        redirect_to bookings_url, notice: 'insufficience credit'
      end
    elsif current_user.profile.company.name == "personal"
      if (current_user.wallet.to_i - (es*3/3600).to_i) > 0
        nbalance = current_user.wallet.to_i - (es*3/3600).to_i
        current_user.update_attribute :wallet, nbalance.to_i
        respond_to do |format|
          if @booking.save
            format.html { redirect_to bookings_url, notice: 'Booking was successfully created.' }
            format.json { render :show, status: :created, location: @booking }
          else
            format.html { render :new }
            format.json { render json: @booking.errors, status: :unprocessable_entity }
          end
        end
      else
        redirect_to bookings_url, notice: 'insufficience credit'
      end
    end

  end

  # PATCH/PUT /bookings/1
  # PATCH/PUT /bookings/1.json
  def update
    respond_to do |format|
      if @booking.update(booking_params)
        format.html { redirect_to bookings_url, notice: 'Booking was successfully updated.' }
        format.json { render :show, status: :ok, location: @booking }
      else
        format.html { render :edit }
        format.json { render json: @booking.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bookings/1
  # DELETE /bookings/1.json
  def destroy
    @booking.destroy
    respond_to do |format|
      format.html { redirect_to bookings_url, notice: 'Booking was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_booking
      @booking = Booking.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def booking_params
      params.require(:booking).permit(:user_id, :room_id, :description, :start_book, :end_book)
    end
end
