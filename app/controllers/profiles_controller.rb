class ProfilesController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_profile, only: [:show, :edit, :update, :destroy]

  # GET /profiles
  # GET /profiles.json

  def index
    @profiles = Profile.where('user_id = ?', current_user)
    @redeems = Redeem.all.page params[:page]
  end

  # GET /profiles/1
  # GET /profiles/1.json
  def show
  end

  # GET /profiles/new
  def new
    if Rails.env.production?
      if current_user.profile.nil?
        @profile = Profile.new
      else
        # raise error which doesn't make sense or redirect like
        redirect_to profiles_path, notice: 'You already have a profile'
      end
    else
      @profile = Profile.new
    end
  end

  # GET /profiles/1/edit
  def edit
  end

  # POST /profiles
  # POST /profiles.json
  def create
    @profile = Profile.new(profile_params)
    @profile.user_id = current_user.id
    respond_to do |format|
      if @profile.save
        create_pjurnal
        format.html { redirect_to "/", notice: 'Profile was successfully created.' }
        format.json { render :show, status: :created, location: @profile }
      else
        format.html { render :new }
        format.json { render json: @profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /profiles/1
  # PATCH/PUT /profiles/1.json
  def update
    respond_to do |format|
      if @profile.update(profile_params)
        format.html { redirect_to profiles_url, notice: 'Profile was successfully updated.' }
        format.json { render :show, status: :ok, location: @profile }
      else
        format.html { render :edit }
        format.json { render json: @profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /profiles/1
  # DELETE /profiles/1.json
  def destroy
    @profile.destroy
    respond_to do |format|
      format.html { redirect_to profiles_url, notice: 'Profile was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def create_pjurnal
    require 'uri'
    require 'net/http'

    if Rails.env.production?
      url = URI("https://api.jurnal.id/core/api/v1/customers?apikey=906dbd5f8453c9a323b6f9b056690c96")
    else
      url = URI("https://sandbox-api.jurnal.id/core/api/v1/customers?apikey=c51a5dfab205e5030e4d7707d12dc42e")
    end

    http = Net::HTTP.new(url.host, url.port)
    if Rails.env.production?
    http.use_ssl = true
    else
    http.use_ssl = true
    end
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    if Rails.env.production?
      if @profile.company.name == "personal"

        if current_space.subdomain == "themaja"
          @payload = {
              "customer": {
              "display_name": @profile.fullname,
              "email": @profile.user.email,
              "default_ar_account": {
                "id": 46940,
                "name": "(1-1201) [The Maja] Revenue Receivable"
              },
              "default_ap_account": {
                "id": 46953,
                "name": "(2-2000) Accounts Payable"
              }
                                } }.to_json
        elsif current_space.subdomain == "breeze"
          @payload = {
              "customer": {
              "display_name": @profile.fullname,
              "email": @profile.user.email,
              "default_ar_account": {
                "id": 761541,
                "name": "(1-1202) [The Breeze] Revenue Receivable"
              },
              "default_ap_account": {
                "id": 46953,
                "name": "(2-2000) Accounts Payable"
              }
                                } }.to_json
        end

        end
    else
    if @profile.company.name == "personal"

      if current_space.subdomain == "themaja"
        @payload = {
            "customer": {
            "display_name": @profile.fullname,
            "email": @profile.user.email,
            "default_ar_account": {
              "id": 46940,
              "name": "(1-1201) [The Maja] Revenue Receivable"
            },
            "default_ap_account": {
              "id": 46953,
              "name": "(2-2000) Accounts Payable"
            }
                              } }.to_json
      elsif current_space.subdomain == "breeze"
        @payload = {
            "customer": {
            "display_name": @profile.fullname,
            "email": @profile.user.email,
            "default_ar_account": {
              "id": 761541,
              "name": "(1-1202) [The Breeze] Revenue Receivable"
            },
            "default_ap_account": {
              "id": 46953,
              "name": "(2-2000) Accounts Payable"
            }
                              } }.to_json
      end
      end
    end

    request = Net::HTTP::Post.new(url)
    request["content-type"] = 'application/json'
    request.body = @payload

    response = http.request(request)
    puts response.read_body
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_profile
      @profile = Profile.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def profile_params
      params.require(:profile).permit(:fullname, :company_id, :phone, :address, :avatar,:admin_id, :user_id, :title)
    end
end
