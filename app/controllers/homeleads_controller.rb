class HomeleadsController < ApplicationController
  before_action :set_homelead, only: [:show, :edit, :update, :destroy]

  # GET /homeleads
  # GET /homeleads.json
  def index
    @homeleads = Homelead.all
  end

  # GET /homeleads/1
  # GET /homeleads/1.json
  def show
  end

  # GET /homeleads/new
  def new
    @homelead = Homelead.new
  end

  # GET /homeleads/1/edit
  def edit
  end

  # POST /homeleads
  # POST /homeleads.json
  def create
    @homelead = Homelead.new(homelead_params)

    respond_to do |format|
      if @homelead.save
        UserMailer.home_lead(@homelead).deliver_now
        format.html { redirect_to root_path, notice: 'Homelead was successfully created.' }
        format.json { render :show, status: :created, location: root_path }
      else
        format.html { render :new }
        format.json { render json: root_path.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /homeleads/1
  # PATCH/PUT /homeleads/1.json
  def update
    respond_to do |format|
      if @homelead.update(homelead_params)
        format.html { redirect_to @homelead, notice: 'Homelead was successfully updated.' }
        format.json { render :show, status: :ok, location: @homelead }
      else
        format.html { render :edit }
        format.json { render json: @homelead.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /homeleads/1
  # DELETE /homeleads/1.json
  def destroy
    @homelead.destroy
    respond_to do |format|
      format.html { redirect_to homeleads_url, notice: 'Homelead was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_homelead
      @homelead = Homelead.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def homelead_params
      params.require(:homelead).permit(:fullname, :phone, :email, :notes)
    end
end
