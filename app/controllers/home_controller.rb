class HomeController < ApplicationController
  def index
    @admins = Admin.all
  	@banners = Banner.all
  	@plans = Plan.all
  	@settings = Setting.all
    @events = Event.all
    @benefits = Benefit.all
    @abouts = About.all
    @testimonials =  Testimonial.all
    @contact = Contact.new
    @homelead = Homelead.new
  end
end
