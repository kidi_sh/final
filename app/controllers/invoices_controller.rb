class InvoicesController < ApplicationController
  before_action :set_invoice, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  def index
    @invoices = current_user.suborders.where('paid != ?',true)
  	# @invoices = current_user.suborders.where('paid = ? and suborders.start_date between ? and ?',false, Date.today.beginning_of_month, Date.today.end_of_month)
    @paid_invoices = current_user.suborders.where('paid = ?',true)
  end

  def show

  end

 private
    # Use callbacks to share common setup or constraints between actions.
  def set_invoice
    @invoice = current_user.suborders.find(params[:id])
  end
end
