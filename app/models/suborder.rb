class Suborder < ActiveRecord::Base
	paginates_per 10
	belongs_to :purchase
	belongs_to :generator
	default_scope {where(admin_id: Admin.current_id) if Admin.current_id}
end
