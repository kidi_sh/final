class Profile < ActiveRecord::Base
	default_scope {where(admin_id: Admin.current_id) if Admin.current_id}

	belongs_to :user
	belongs_to :company

	has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" },    :path => ":rails_root/public/images/user/:class/:attachment/:id/:style/:filename",:url => "/images/user/:class/:attachment/:id/:style/:filename"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/

	validates :fullname , presence: true
	validates :company_id, presence: true
	validates :phone, presence: true
	validates :address, presence: true
end
