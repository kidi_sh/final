class Booking < ActiveRecord::Base
	paginates_per 8
	belongs_to :user
	belongs_to :room

	default_scope {where(admin_id: Admin.current_id) if Admin.current_id}

	validate :booking_period_not_overlapped
  	validate :date_validation
	validate :daily_booking_limit
	validate :hour_validation

	  private

			def daily_booking_limit
				if user.profile.company.name != "EV Hive"
					dbl = Booking.select('sum(EXTRACT(EPOCH FROM (end_book-start_book))/3600) as total').where('user_id = ? and created_at between ?  and  ?', user.id,Date.today.beginning_of_day , Date.today.end_of_day)
					if dbl.empty?
						return true
					else
						nv = dbl.map{|f|f.total}.reduce(0,:+).to_i
						if nv > 2
							errors.add(:status,': You have no more daily booking quota')
							return false
						else
							return true
						end
					end
				end
			end

			def week_booking_limit
				if user.profile.company.name == "personal"
					a = Booking.select('sum(EXTRACT(EPOCH FROM (end_book-start_book))/3600) as total').where('user_id = ? and created_at between ?  and  ?', user.id,Date.today.beginning_of_week, Date.today.end_of_week)
					if a.empty?
						return true
					else
						b = a.map{|f|f.total}.reduce(0,:+).to_i
						c = b + ((self[:end_book] - self[:start_book]).to_i / 3600)
						if c > 10
							errors.add(:status,': You have no more weekly booking quota')
							return false
						else
							return true
						end
					end
				else
					return true
				end
			end

	    def booking_period_not_overlapped

				st = Booking.where('(admin_id = ? AND room_id = ? AND start_book >= ? AND start_book <= ?)',admin_id,room_id,start_book,end_book)
				mt = Booking.where('(admin_id = ? AND room_id = ? AND start_book <= ? AND end_book >= ?)',admin_id,room_id,start_book,end_book)
				et = Booking.where('(admin_id = ? AND room_id = ? AND end_book >= ? AND end_book <= ?)',admin_id,room_id,start_book,end_book)

				if !st.empty?
					errors.add(:error, ': Time Unavailable')
					return false
				elsif !mt.empty?
					errors.add(:error, ': Time Unavailable')
					return false
				elsif !et.empty?
					errors.add(:error, ': Time Unavailable')
					return false
				else
					return true
				end


	      # unless
	      #   Booking.where('(admin_id = ? AND room_id = ? AND start_book >= ? AND start_book <= ?)',admin_id,room_id,start_book,end_book).empty?
	      #   errors.add(:error, ': End time Unavailable')
	      # end
				#
				# unless
				# 	Booking.where('(admin_id = ? AND room_id = ? AND start_book <= ? AND end_book >= ?)',admin_id,room_id,start_book,end_book).empty?
				# 	errors.add(:error, ': Start and end time overlap')
				# end
				#
				# unless
				# 	Booking.where('(admin_id = ? AND room_id = ? AND end_book >= ? AND end_book <= ?)',admin_id,room_id,start_book,end_book).empty?
				# 	errors.add(:error, ': Start time Unavailable')
				# end

	    end

	    def date_validation
		    if self[:end_book] < self[:start_book]
		      errors[:end_book] << "must be greater"
		      return false
		    else
		      return true
		    end
	  	end

		def hour_validation
			if ((self[:start_book]-self[:end_book])/3600) > 2
				errors[:end_book] << "max period is 2 hour"
				errors[:start_book] << "max period is 2 hour"
				return false
			else
				return true
			end
		end
end
