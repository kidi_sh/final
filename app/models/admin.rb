class Admin < ActiveRecord::Base
default_scope {where(id: Admin.current_id) if Admin.current_id}
validates :subdomain, presence: true, on: :create, :uniqueness => true
#cattr_accessor :current_id
has_one :setting
has_one :about
has_many :users

def self.current_id=(id)
	Thread.current[:admin_id] = id
end

def self.current_id
	Thread.current[:admin_id]
end
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end
