class Purchase < ActiveRecord::Base
	default_scope {where(admin_id: Admin.current_id) if Admin.current_id}

	validate :purchase_period_not_overlapped
	validates :plan_id, presence: true
	validates :payment_id, presence: true
	# validates :total_month, presence: true
	belongs_to :room
	belongs_to :plan
	belongs_to :payment

	belongs_to :event

	belongs_to :user
	has_many :suborders

	private
		def purchase_period_not_overlapped
			if plan.period == 4

				unless
					Booking.where(
					' (admin_id = ? AND room_id = ? AND start_book >= ? AND start_book <= ?)',
					admin_id, self.room_id ,self.start_date, self.start_date + self.total_month.to_i.hours
					).empty?
					errors.add('Selected time', ' unavailable')
				end

				unless
					Booking.where(
						'(admin_id= ? AND room_id = ? AND start_book <= ? AND end_book >= ?)',
						admin_id, self.room_id ,self.start_date, self.start_date + self.total_month.to_i.hours
						).empty?
						errors.add('Selected time', ' unavailable')
				end

				unless
					Booking.where(
					' (admin_id = ? AND room_id = ? AND end_book >= ? AND end_book <= ?)',
					admin_id, self.room_id ,self.start_date, self.start_date + self.total_month.to_i.hours
					).empty?
					errors.add('Selected time', ' unavailable')
				end

				unless
					Booking.where(
					' (admin_id = ? AND room_id = ? AND start_book >= ? AND end_book <= ?)',
					admin_id, self.room_id ,self.start_date, self.start_date + self.total_month.to_i.hours
					).empty?
					errors.add('Selected time', ' unavailable')
				end




			end
		end

end
