class Contact < ActiveRecord::Base
  default_scope {where(admin_id: Admin.current_id) if Admin.current_id}
  belongs_to :admin
end
