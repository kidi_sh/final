class Generator < ActiveRecord::Base
    default_scope {where(admin_id: Admin.current_id) if Admin.current_id}
    has_many :suborders

    def search_suborders
      suborders = Suborder.all
      suborders = Suborder.select('sum(suborders.total/plans.price/plans.deposit) as myval, plans.name as plan_name').group('plans.name').joins('left join purchases on suborders.purchase_id = purchases.id').joins('left join plans on purchases.plan_id = plans.id').where('suborders.start_date between ? and ? and plans.name = ? ' ,  start_date.beginning_of_month, start_date.end_of_month, plan_name)

      return suborders
    end
end
