class About < ActiveRecord::Base
  belongs_to :admin
  default_scope {where(admin_id: Admin.current_id) if Admin.current_id}

  has_attached_file :logo, styles: { medium: "300x300>", thumb: "100x100>" },    :path => ":rails_root/public/images/admin/:class/:attachment/:id/:style/:filename",:url => "/images/admin/:class/:attachment/:id/:style/:filename"
    validates_attachment_content_type :logo, content_type: /\Aimage\/.*\z/

  has_attached_file :icon_1, styles: { medium: "300x300>", thumb: "100x100>" },    :path => ":rails_root/public/images/admin/:class/:attachment/:id/:style/:filename",:url => "/images/admin/:class/:attachment/:id/:style/:filename"
  	validates_attachment_content_type :icon_1, content_type: /\Aimage\/.*\z/
  has_attached_file :icon_2, styles: { medium: "300x300>", thumb: "100x100>" },    :path => ":rails_root/public/images/admin/:class/:attachment/:id/:style/:filename",:url => "/images/admin/:class/:attachment/:id/:style/:filename"
  	validates_attachment_content_type :icon_2, content_type: /\Aimage\/.*\z/
  has_attached_file :icon_3, styles: { medium: "300x300>", thumb: "100x100>" },    :path => ":rails_root/public/images/admin/:class/:attachment/:id/:style/:filename",:url => "/images/admin/:class/:attachment/:id/:style/:filename"
  	validates_attachment_content_type :icon_3, content_type: /\Aimage\/.*\z/
  has_attached_file :icon_4, styles: { medium: "300x300>", thumb: "100x100>" },    :path => ":rails_root/public/images/admin/:class/:attachment/:id/:style/:filename",:url => "/images/admin/:class/:attachment/:id/:style/:filename"
  	validates_attachment_content_type :icon_4, content_type: /\Aimage\/.*\z/
end
