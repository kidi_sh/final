class Banner < ActiveRecord::Base
	default_scope {where(admin_id: Admin.current_id) if Admin.current_id}

	has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" },    :path => ":rails_root/public/images/admin/:class/:attachment/:id/:style/:filename",:url => "/images/admin/:class/:attachment/:id/:style/:filename"
  	validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

end
