class Comment < ActiveRecord::Base
default_scope {where(admin_id: Admin.current_id) if Admin.current_id}
  belongs_to :forum
  belongs_to :user
end
