class Forum < ActiveRecord::Base
	default_scope {where(admin_id: Admin.current_id) if Admin.current_id}
	belongs_to :user
	has_many :comments
end
