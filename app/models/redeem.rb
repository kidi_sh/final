class Redeem < ActiveRecord::Base
  default_scope {where(admin_id: Admin.current_id) if Admin.current_id}
  # has_one :benefit
  # has_one :benefit
  paginates_per 6

  belongs_to :benefit
end
