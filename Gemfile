source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.6'
# Use postgresql as the database for Active Record
gem 'pg', '~> 0.15'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

gem 'devise', '~> 4.2'

gem 'twitter-bootstrap-rails', :git => 'git://github.com/seyhunak/twitter-bootstrap-rails.git'

gem 'bootstrap-datepicker-rails', :require => 'bootstrap-datepicker-rails',
                              :git => 'git://github.com/Nerian/bootstrap-datepicker-rails.git'

gem 'momentjs-rails', '>= 2.9.0'
gem 'bootstrap3-datetimepicker-rails', '~> 4.17.42'

gem 'fullcalendar-rails'

gem 'jquery-turbolinks', '~> 2.1'

gem 'business_time', '~> 0.7.6'
gem 'chronic', '~> 0.10.2'

gem 'devise-bootstrap-views'
gem 'kaminari'
gem 'bootstrap-kaminari-views'
gem 'bootstrap-wysihtml5-rails', github: 'nerian/bootstrap-wysihtml5-rails'
gem 'wicked', '~> 1.3', '>= 1.3.1'

gem 'paperclip', '~> 5.1'

gem 'activeadmin', github: 'activeadmin'
gem 'active_admin_theme'
gem 'active_skin'
gem "introjs-rails"

gem 'ckeditor'
gem 'friendly_id', '~> 5.1.0'

gem 'owlcarousel-rails'

gem 'wicked_pdf', '~> 1.1'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'
gem 'truncate_html', '~> 0.9.3'
gem 'time_diff', '~> 0.3.0'
# Use Unicorn as the app server
# gem 'unicorn'
gem 'social-share-button'
gem 'select2-rails', '~> 4.0', '>= 4.0.3'

gem 'jquery-datatables-rails', '~> 3.4.0'

gem "recaptcha", require: "recaptcha/rails"

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development


# Add this if you're using rvm
# gem 'capistrano-rvm', github: "capistrano/rvm"

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
  # Add this if you're using rbenv

end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'
  gem 'capistrano-rbenv', '~> 2.0'
  gem 'capistrano', '~> 3.6', '>= 3.6.1'
  gem 'capistrano-bundler', '~> 1.1.2'
  gem 'capistrano-rails', '~> 1.2'
  gem 'capistrano-passenger'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
end
