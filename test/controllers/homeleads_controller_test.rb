require 'test_helper'

class HomeleadsControllerTest < ActionController::TestCase
  setup do
    @homelead = homeleads(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:homeleads)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create homelead" do
    assert_difference('Homelead.count') do
      post :create, homelead: { email: @homelead.email, fullname: @homelead.fullname, notes: @homelead.notes, phone: @homelead.phone }
    end

    assert_redirected_to homelead_path(assigns(:homelead))
  end

  test "should show homelead" do
    get :show, id: @homelead
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @homelead
    assert_response :success
  end

  test "should update homelead" do
    patch :update, id: @homelead, homelead: { email: @homelead.email, fullname: @homelead.fullname, notes: @homelead.notes, phone: @homelead.phone }
    assert_redirected_to homelead_path(assigns(:homelead))
  end

  test "should destroy homelead" do
    assert_difference('Homelead.count', -1) do
      delete :destroy, id: @homelead
    end

    assert_redirected_to homeleads_path
  end
end
