class AddOccupancyToSuborders < ActiveRecord::Migration
  def change
    add_column :suborders, :occupancy, :decimal
  end
end
