class AddMetaDescriptionToBlogs < ActiveRecord::Migration
  def change
    add_column :blogs, :meta_description, :string
  end
end
