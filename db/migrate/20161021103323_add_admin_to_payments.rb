class AddAdminToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :admin_id, :integer
    add_index :payments, :admin_id
  end
end
