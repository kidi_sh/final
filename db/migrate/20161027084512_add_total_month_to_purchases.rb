class AddTotalMonthToPurchases < ActiveRecord::Migration
  def change
    add_column :purchases, :total_month, :integer
    add_column :purchases, :start_date, :datetime
  end
end
