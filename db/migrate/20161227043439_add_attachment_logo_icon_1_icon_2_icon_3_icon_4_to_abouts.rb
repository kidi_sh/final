class AddAttachmentLogoIcon1Icon2Icon3Icon4ToAbouts < ActiveRecord::Migration
  def self.up
    change_table :abouts do |t|
      t.attachment :logo
      t.attachment :icon_1
      t.attachment :icon_2
      t.attachment :icon_3
      t.attachment :icon_4
    end
  end

  def self.down
    remove_attachment :abouts, :logo
    remove_attachment :abouts, :icon_1
    remove_attachment :abouts, :icon_2
    remove_attachment :abouts, :icon_3
    remove_attachment :abouts, :icon_4
  end
end
