class AddEndDateToPurchase < ActiveRecord::Migration
  def change
    add_column :purchases, :end_date, :datetime
  end
end
