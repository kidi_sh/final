class AddAdminIdToGenerator < ActiveRecord::Migration
  def change
    add_column :generators, :admin_id, :integer
  end
end
