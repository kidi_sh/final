class AddAdminIdToPurchases < ActiveRecord::Migration
  def change
    add_column :purchases, :admin_id, :integer
    add_index :purchases, :admin_id
  end
end
