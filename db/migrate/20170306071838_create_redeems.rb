class CreateRedeems < ActiveRecord::Migration
  def change
    create_table :redeems do |t|
      t.datetime :date
      t.integer :user_id
      t.integer :benefit_id

      t.timestamps null: false
    end
  end
end
