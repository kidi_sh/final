class CreatePurchases < ActiveRecord::Migration
  def change
    create_table :purchases do |t|
      t.integer :user_id
      t.integer :plan_id
      t.integer :payment_id
      t.text :notes

      t.timestamps null: false
    end
  end
end
