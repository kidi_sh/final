class AddInterestToHelpdesks < ActiveRecord::Migration
  def change
    add_column :helpdesks, :interest, :string
    add_column :helpdesks, :source, :text
  end
end
