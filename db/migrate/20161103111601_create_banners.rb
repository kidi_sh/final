class CreateBanners < ActiveRecord::Migration
  def change
    create_table :banners do |t|
      t.string :title
      t.text :description
      t.integer :admin_id
      t.timestamps null: false
    end
  end
end
