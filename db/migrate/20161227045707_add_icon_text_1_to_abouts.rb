class AddIconText1ToAbouts < ActiveRecord::Migration
  def change
    add_column :abouts, :icon_text_1, :string
    add_column :abouts, :icon_text_2, :string
    add_column :abouts, :icon_text_3, :string
    add_column :abouts, :icon_text_4, :string
  end
end
