class AddCompanyIdToHelpdesks < ActiveRecord::Migration
  def change
    add_column :helpdesks, :company_id, :integer
  end
end
