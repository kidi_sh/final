class CreateHomeleads < ActiveRecord::Migration
  def change
    create_table :homeleads do |t|
      t.string :fullname
      t.string :phone
      t.string :email
      t.text :notes

      t.timestamps null: false
    end
  end
end
