class AddWeekendPriceToPlans < ActiveRecord::Migration
  def change
    add_column :plans, :weekend_price, :decimal
    add_column :plans, :weekend_name, :string
  end
end
