class CreateHelpdesks < ActiveRecord::Migration
  def change
    create_table :helpdesks do |t|
      t.string :full_name
      t.string :company
      t.string :phone
      t.string :email
      t.integer :user_id
      t.text :note
      t.integer :admin_id

      t.timestamps null: false
    end
  end
end
