class AddPaidToSuborders < ActiveRecord::Migration
  def change
    add_column :suborders, :paid, :boolean, :default => false
  end
end
