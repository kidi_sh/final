class CreateBookings < ActiveRecord::Migration
  def change
    create_table :bookings do |t|
      t.integer :user_id
      t.integer :room_id
      t.text :description
      t.datetime :start_book
      t.datetime :end_book

      t.timestamps null: false
    end
  end
end
