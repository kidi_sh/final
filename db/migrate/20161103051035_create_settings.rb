class CreateSettings < ActiveRecord::Migration
  def change
    create_table :settings do |t|
      t.string :name
      t.text :address
      t.string :phone
      t.integer :admin_id

      t.timestamps null: false
    end
  end
end
