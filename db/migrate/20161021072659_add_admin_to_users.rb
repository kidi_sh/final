class AddAdminToUsers < ActiveRecord::Migration
  def change
    add_column :users, :admin_id, :integer
    add_column :users, :index, :string
  end
end
