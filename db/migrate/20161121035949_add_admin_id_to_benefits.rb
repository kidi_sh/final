class AddAdminIdToBenefits < ActiveRecord::Migration
  def change
    add_column :benefits, :admin_id, :integer
    add_index :benefits, :admin_id
  end
end
