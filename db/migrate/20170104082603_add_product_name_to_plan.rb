class AddProductNameToPlan < ActiveRecord::Migration
  def change
    add_column :plans, :product_name, :string
  end
end
