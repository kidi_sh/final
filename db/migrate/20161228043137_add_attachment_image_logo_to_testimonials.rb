class AddAttachmentImageLogoToTestimonials < ActiveRecord::Migration
  def self.up
    change_table :testimonials do |t|
      t.attachment :image
      t.attachment :logo
    end
  end

  def self.down
    remove_attachment :testimonials, :image
    remove_attachment :testimonials, :logo
  end
end
