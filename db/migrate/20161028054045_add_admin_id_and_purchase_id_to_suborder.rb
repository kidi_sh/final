class AddAdminIdAndPurchaseIdToSuborder < ActiveRecord::Migration
  def change
    add_column :suborders, :admin_id, :integer
    add_index :suborders, :admin_id
    add_column :suborders, :purchase_id, :integer
  end
end
