class AddAdminToPlans < ActiveRecord::Migration
  def change
    add_column :plans, :admin_id, :integer
    add_index :plans, :admin_id
  end
end
