class AddAboutToSettings < ActiveRecord::Migration
  def change
    add_column :settings, :about, :text
  end
end
