class AddAttachmentPimageToPlans < ActiveRecord::Migration
  def self.up
    change_table :plans do |t|
      t.attachment :pimage
    end
  end

  def self.down
    remove_attachment :plans, :pimage
  end
end
