class RemoveIndexFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :index, :string
  end
end
