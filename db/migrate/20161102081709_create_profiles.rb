class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :fullname
      t.integer :company_id
      t.string :phone
      t.text :address
      t.integer :admin_id
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
