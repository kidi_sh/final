class CreateSuborders < ActiveRecord::Migration
  def change
    create_table :suborders do |t|
      t.datetime :start_date
      t.datetime :end_date
      t.integer :days
      t.decimal :total

      t.timestamps null: false
    end
  end
end
