class AddAdminToRooms < ActiveRecord::Migration
  def change
    add_column :rooms, :admin_id, :integer
    add_index :rooms, :admin_id
  end
end
