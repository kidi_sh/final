class AddExtraChargeToPlan < ActiveRecord::Migration
  def change
    add_column :plans, :extracharge, :decimal
  end
end
