class AddProductNameToSuborder < ActiveRecord::Migration
  def change
    add_column :suborders, :product_name, :string
  end
end
