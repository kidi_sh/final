class AddAdminToEvents < ActiveRecord::Migration
  def change
    add_column :events, :admin_id, :integer
    add_index :events, :admin_id
  end
end
