class AddAdminToBookings < ActiveRecord::Migration
  def change
    add_column :bookings, :admin_id, :integer
    add_column :bookings, :index, :string
  end
end
