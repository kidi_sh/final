class AddGoogleMapsToSetting < ActiveRecord::Migration
  def change
    add_column :settings, :google_maps, :string
  end
end
