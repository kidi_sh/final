class CreateGenerators < ActiveRecord::Migration
  def change
    create_table :generators do |t|
      t.string :plan_name
      t.date :start_date

      t.timestamps null: false
    end
  end
end
