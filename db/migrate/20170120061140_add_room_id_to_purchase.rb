class AddRoomIdToPurchase < ActiveRecord::Migration
  def change
    add_column :purchases, :room_id, :integer
  end
end
